
/**
 * @Package: Gifto MetaMask - Transfer Gifto coin with MetaMask
 * Create by: TuanNguyen
 */

var GiftoABI = [{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"totalSupply","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"digits","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}]
var GiftoAddress = "0xC5bBaE50781Be1669306b9e001EFF57a2957b09d";


/*------------------------------------------
DEFINE GIFTO METAMASK
------------------------------------------*/
function GIFTO_METAMASK (){
	
	this.web3 = null;
	this.wallet = null;

	// Checking if Web3 has been injected by the browser (Mist/MetaMask)
	if (typeof web3 !== 'undefined') {
		// Use Mist/MetaMask's provider
		this.web3 = new Web3(web3.currentProvider);
		
	} else {
		console.log('No web3? You should consider trying MetaMask!');
	}
};
/*------------------------------------------
	CHECK EXIST WEB3
------------------------------------------*/
GIFTO_METAMASK.prototype.validateWeb3 = function () {
	if (typeof web3 == 'undefined') {
		this.showErrorMessage('Please install MetaMask extension on your browser!');
		return false;
	} else {
		return true;
	}
};
/*------------------------------------------
	GET AND SUBMIT GIFTO TRANSFER
------------------------------------------*/
GIFTO_METAMASK.prototype.gwTransferGifto = function (form_id) {
	// debugger;
	var $this = this;

	if ( form_id == undefined || form_id == null ) {
				
		console.error('Tranfer Gifto missing formId');
		return false;

	} else {

		var FormApi = document.getElementById(form_id);
		var formData = new FormData(FormApi);

		if ( this.validateFromData(formData) ) {

			var formValidatedData = {
				"senderAddress": null,
				"senderName": formData.get('gifto_sender_name'),
				"senderEmail": formData.get('gifto_sender_email'),
				"receiverAddress": formData.get('gifto_recipient_wallet_address'),
				"receiverEmail": formData.get('gifto_recipient_email'),
				"amount": this.formatAmount(formData.get('gifto_amount')),
				"transactionHash": null,
				"greetingMessage" : formData.get('gifto_message')
			};





			if (this.validateWeb3()) {
				var $this = this
				this.web3.eth.getAccounts(function (err, accounts) {
				
					if (accounts.length == 0) {
						
						$this.showErrorMessage('Please sign in or create your MetaMask account !');

					} else {
						formValidatedData.senderAddress = accounts[0];
						GIFTO_METAMASK.prototype.sendGifto(
							formValidatedData.senderAddress, 
							formValidatedData.receiverAddress,
							formValidatedData.amount).then(function(txHash){
								$this.sendEmail(
									formValidatedData.senderEmail,
									formValidatedData.receiverEmail,
									formValidatedData.greetingMessage || "Merry Christmas and Best wishes for a Happy New Year 2018",
									formValidatedData.amount / (100000),
									txHash,
									formValidatedData.senderName,
								);


								$this.storeTransactionToDatabase({
									senderName : formValidatedData.senderName,
									senderAddress : formValidatedData.senderAddress,
									senderEmail : formValidatedData.senderEmail,
									recipientAddress : formValidatedData.receiverAddress,
									recipientEmail : formValidatedData.receiverEmail,
									amount : formValidatedData.amount / (100000),
									transactionHash : txHash,
									message : formValidatedData.greetingMessage,
								})
							})
						// console.log(accounts);
						// var wallet = accounts[0];
						// var metamaskObj = {
						// 	"from": wallet,
						// 	"to": formValidatedData.to,
						// 	"value": requestApi.value,
						// 	"data": requestApi.message,
						// 	"chainId":1
						// };
						// web3.eth.sendTransaction(metamaskObj, function(err, transactionHash){
						// 	if (err) {
						// 		$this.showErrorMessage(err);
						// 	} else {
						// 		console.log(transactionHash);
						// 	}
						// });
					}
				});
			}

			return true;

		}

		
	}
};
/*------------------------------------------
	SEND TRANSACTION
------------------------------------------*/
GIFTO_METAMASK.prototype.sendGifto = function (from, to, amount) {
	$this = this;
	var giftoContract = web3.eth.contract(GiftoABI);
	var giftoInstance = giftoContract.at(GiftoAddress);

	var resolve = null;
	var reject = null;

	//== make sure sender balance enough
	giftoInstance.balanceOf.call(from, function (err, result) {
		if (err) {
			$this.showErrorMessage(err);
		} else {
			console.log(result);
			result.c[0] = 10000000000;
			if (result.c[0] < amount) {
				$this.showErrorMessage('Your gifto balance not enough, please enter another value');
			} else {
				// transfer gifto
				giftoInstance.transfer(to, amount, { value: 0, gas: 100000 }, function (err, result) {
					if (err) {
						$this.showErrorMessage(err);
						reject(err);
					} else {
						resolve(result);
					}
				});
			}
		}
	});

	return new Promise(function(_resolve,_reject){
		reject = _reject;
		resolve = _resolve;
	});
};

function b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
        function toSolidBytes(match, p1) {
            return String.fromCharCode('0x' + p1);
    }));
}


/*------------------------------------------
	SEND EMAIL
------------------------------------------*/
GIFTO_METAMASK.prototype.sendEmail = function (senderEmail, receiverEmail, greetingMessage, amount, txHash, senderName) {
	var data = {
		"senderEmail": senderEmail,
		"receiverEmail": receiverEmail,
		"greetingMessage" : b64EncodeUnicode(greetingMessage),
		"amount" : amount,
		"transactionHash" : txHash,
		"senderName": senderName,
	};
	var mailServer = 'https://store-testing.gifto.io/gift/emails/send';
	var methodAjax = 'POST';


	// var formData = new FormData()

	// for(var i in data)
	// 	formData.append(i,data[i])

	// fetch("https://store-testing.gifto.io/gift/emails/send", {
	// 	method: 'POST',
	// 	// credentials: 'same-origin',
	// 	body : formData,
		
	// })

	jQuery.ajax({
		method: methodAjax,
		url: mailServer,
		data: JSON.stringify(data),
		crossDomain: true,
		contentType: "application/json",
		dataType: "json",
	}).done(function( msg ) {
		console.log(msg);
	});
};
/*------------------------------------------
	STORE SUCCESSFUL TRANSACTION
------------------------------------------*/
GIFTO_METAMASK.prototype.storeTransactionToDatabase = function (options) {
	var data = {
		"senderName": options.senderName,
		"senderAddress": options.senderAddress,
		"senderEmail": options.senderEmail,
		"receiverAddress": options.recipientAddress,
		"receiverEmail": options.recipientEmail,
		"amount": options.amount,
		"transactionHash": options.transactionHash,
		"greetingMessage" : options.message
	};
	
	var methodAjax = 'POST';

	jQuery.ajax({
		method: methodAjax,
		url: 'https://store-testing.gifto.io/gift/transactions/submit',
		data: JSON.stringify(data),
		crossDomain: true,
		contentType: "application/json",
		dataType: "json",
	})
	.done(function( msg ) {
		console.log(msg);
	});
};
/*------------------------------------------
	VALIDATION
------------------------------------------*/
GIFTO_METAMASK.prototype.validateFromData = function (formData) {
	var $this = this;
		var isValid = true;
		console.log(formData);

	//== Validate Sender Name
	if (!$this.validateRequired(formData.get('gifto_sender_name'))) {
		$this.showErrorMessage("Please enter Your Name.");
		isValid = false;
	}

	//== Validate Sender Email
	if ( $this.validateEmail(formData.get('gifto_sender_email')) == -1) {
		$this.showErrorMessage("Please enter Sender Email.");
		isValid = false;
	} else if (!$this.validateEmail(formData.get('gifto_sender_email')) ) {
		$this.showErrorMessage('Please enter with right email format for Sender Email.');
		isValid = false;
	}

	//== Validate Recipient Email
	if ( $this.validateEmail(formData.get('gifto_recipient_email')) == -1) {
		$this.showErrorMessage('Please enter Recipient Email.');
		isValid = false;
	} else if (!$this.validateEmail(formData.get('gifto_recipient_email')) ) {
		$this.showErrorMessage('Please enter with right email format for Recipient Email.');
		isValid = false;
	}

	//== Validate Wallet
	if ( $this.validateWallet(formData.get('gifto_recipient_wallet_address')) == -1) {
		$this.showErrorMessage('Please enter Wallet Address.');
		isValid = false;
	} 

	//  else if (!$this.validateEmail(formData.get('gifto_recipient_wallet_address')) ) {
	// 	$this.showErrorMessage('Please enter with right wallet address.');
	// 	isValid = false;
	// }

	//== Validate Amount
	if ($this.validateNumber(formData.get('gifto_amount')) == -1) {
		$this.showErrorMessage('Please enter Amount of Coin.');
		isValid = false;
	}

	return isValid;
};
/**
 *	Validation: Required Validate
	*/
GIFTO_METAMASK.prototype.validateRequired = function (value) {
	if ( value == undefined || value == null || (typeof value == "string" && value.trim().length == 0 ) ) {
		return false;
	} else {
		return true;
	}
}
/**
 *	Validation: Nummeric Validate
	*/
GIFTO_METAMASK.prototype.validateNumber = function (value) {
	if ( value == undefined || value == null || typeof parseInt(value) != "number" ) {
		return false;
	} else {
		return true;
	}
}
/**
 *	Validation: Gifto Email
	*/
GIFTO_METAMASK.prototype.validateEmail = function (value) {
	if ( value == undefined || value == null || (typeof value == "string" && value.trim().length == 0 ) ) {
		return -1;
	} else {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
		return re.test(value);
	}
}
/**
 *	Validation: Wallet
	*/
GIFTO_METAMASK.prototype.validateWallet = function (value) {
	if ( value == undefined || value == null || (typeof value == "string" && value.trim().length == 0 ) ) {
		return -1;
	} else {
		var re = /^0x[0-9A-Za-z]{40}$/i;

		return re.test(value);
	}
}

/*------------------------------------------
	AMOUNT FORMAT
------------------------------------------*/
GIFTO_METAMASK.prototype.formatAmount = function (amount) {
	if (!this.validateNumber(amount)) {
		this.showErrorMessage('Please enter with right Amount.');
		return false;
	} else {
		return Math.ceil(amount) * 100000;	
	}
		
}
/*------------------------------------------
	SHOW ERROR MESSAGE
------------------------------------------*/
GIFTO_METAMASK.prototype.showErrorMessage = function (message){
	UIkit.notification({message: message, status: 'danger'});
};
